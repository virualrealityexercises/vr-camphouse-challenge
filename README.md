# VR Base Project

Proyecto challenge del path Create with VR de Unity

## Getting started

### Branches

- Main: The main branch

## Add your files

- [ ] [Create](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-file) or [upload](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#upload-a-file) files
- [ ] [Add files using the command line](https://docs.gitlab.com/ee/gitlab-basics/add-file.html#add-a-file-using-the-command-line) or push an existing Git repository with the following command:

```
cd existing_repo
git remote add origin https://gitlab.com/virualrealityexercises/vr-base-project.git
git branch -M main
git push -uf origin main
```

## Authors and acknowledgment
Santiago Garcia Proaño
Linkedin: https://www.linkedin.com/in/santiago-garcia-0b15a81a5/

## License
For open source projects, say how it is licensed.
